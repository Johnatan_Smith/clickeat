import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Gender } from 'src/app/shared/enums/gender';
import { Store } from '@ngrx/store';
import { State } from 'src/app/shared/interfaces/state';
import { LoaderService } from '../shared/services/loader.service';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { FormService } from '../shared/services/form.service';
import { _createUnicorn } from '../shared/functions/_createUnicorn';
import { setTheme } from '../shared/actions/theme/setTheme.action';
import { createMates } from '../shared/actions/unicorns/createMates.action';
import { ActionUnicorn } from '../shared/interfaces/actionUnicorn.interface';
import { createUnicorn } from '../shared/actions/unicorns/createUnicorn.action';
import { resetScene } from '../shared/actions/scene/resetScene.action';
import { themeSelector } from '../shared/selectors/unicorn.selector';

@Component({
  selector: 'app-controls',
  templateUrl: './controls.component.html',
  styleUrls: ['./controls.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ControlsComponent implements OnDestroy {

  fg:FormGroup = new FormGroup( {} );
  secondFg:FormGroup = new FormGroup( {} );
  
  isSingle:boolean = true;

  isDark$:Observable<boolean> = this.store.select( themeSelector )
  
  themeSub!:Subscription;

  haveBaby:boolean = false;

  constructor(

    private store:Store<State>,
    private loader:LoaderService,
    private form:FormService

  ) {

  }

  resetAll ():void {

    this.store.dispatch( resetScene() );

  }

  onChange ( value:boolean ) {

    this.store.dispatch( setTheme( { payload: value } ) );

  }

  resetForm ( loading:boolean ):boolean {
    
    if ( !loading ) {
      
      this.fg.reset();
      this.secondFg.reset();

    }
    
    return ( loading );

  }


  triggerEndOfProcess () {

    this.form.submitForm$.next( true );

    this.loading$.next( false );

  }

  toggleBabyValue ( value:boolean ) {

    this.haveBaby = value;

  }

  getFormValues (form:FormGroup):ActionUnicorn {

    const name = form.controls.name.value;
    const color = form.controls.color.value.hex;
    const gender = <Gender>form.controls.gender.value;
    const age = form.controls.age.value;

    return { name, color, gender, age };
  
  }

  onSubmit ():void {

    const unicorn = this.getFormValues( this.fg );
    const haveBaby = this.haveBaby;

    if ( this.isSingle ) {

      this.store.dispatch( createUnicorn( { payload:unicorn } ) );

    } else {

      const mate = this.getFormValues( this.secondFg );

      this.store.dispatch( createMates( { payload: { unicorn, mate, haveBaby } } ) );

    }

  }

  get loading$ ():BehaviorSubject<boolean> {
    
    return ( this.loader.loading$ )
  
  }

  goToLink ( url:string ):void {

      window.open( url );

  }

  onTabChange(event: MatTabChangeEvent ):void {

    this.isSingle = !event.index;

  }

  isInvalid () {

    return ( 
      this.isSingle ?
      this.fg.untouched || this.fg.invalid :
      this.secondFg.untouched || this.secondFg.invalid
    );

  }

  ngOnDestroy ():void {

  }

}
