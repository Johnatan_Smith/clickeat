import { AfterContentInit, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnDestroy, Renderer2, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { map, skipWhile, take, tap } from 'rxjs/operators';
import { CacheService } from '../shared/services/cache.service';
import { LoaderService } from '../shared/services/loader.service';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SplashComponent implements AfterViewInit, AfterContentInit, OnDestroy {

  @ViewChild('cover', ) cover!:ElementRef<HTMLDivElement>;

  loadingSub!:Subscription;
  cacheSub!:Subscription;

  currentInterval!:any;

  messages:string[] = [
    "Loading Project...",
    "That's a big one!",
    "Go grab a coffee!",
    "You sure love unicorns!",
    "If I was you I'll go to sleep!"
  ];

  message:string = "Pre Loading Unicorns...";

  constructor(

    private loader:LoaderService,
    private renderer:Renderer2,
    private cache:CacheService,
    private cdr:ChangeDetectorRef

  ) {

  }

  addAnimationClass () {

    this.renderer.addClass( this.cover.nativeElement, 'animate' );

  }

  removeAnimationClass () {

    this.renderer.removeClass( this.cover.nativeElement, 'animate' );

  }

  get loading$ ():Observable<boolean> {

    return ( this.loader.loadingCanvas$ );
  
  }

  setMessage () {

    let index = 0;
    
    this.message = this.messages[ index++ ];
    this.cdr.detectChanges();

    this.currentInterval = setInterval( () => {

      if ( index === 5 )
        return ( clearInterval( this.currentInterval ) );

      this.message = this.messages[ index++ ];
      this.cdr.detectChanges();
    
    }, 5000 );

  }

  endSplashScreen () {

    this.message = '';
    clearInterval( this.currentInterval );
    this.addAnimationClass();
    setTimeout( this.endAnimation, 1000 );

  }

  endAnimation = () => {

    this.removeAnimationClass();
    this.loader.loadingProject$.next( false );

  }

  goToLink ( url:string ):void {

    window.open( url );

  }

  ngAfterViewInit(): void {

    this.loadingSub = this.loader.loadingCanvas$.pipe(

      skipWhile( isLoading => isLoading ),
      take( 1 ),
      map( _ => this.endSplashScreen() )

    ).subscribe();
  
  }

  ngAfterContentInit () {

    this.cacheSub = this.cache.isCached$.pipe(
      skipWhile( isCached => !isCached ),
      take( 1 ),
      tap( _ => this.setMessage() )
    ).subscribe()
    
  }

  ngOnDestroy ():void {

    if ( this.loadingSub )
      this.loadingSub.unsubscribe();

    if ( this.cacheSub )
      this.cacheSub.unsubscribe();
  
  }

}
