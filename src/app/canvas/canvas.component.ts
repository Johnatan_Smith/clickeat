import { AfterViewInit, Component, ElementRef, AfterContentInit, OnDestroy, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ThreeService } from 'src/app/shared/services/three.service';
import { resetScene } from '../shared/actions/scene/resetScene.action';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.sass']
})
export class CanvasComponent implements OnDestroy {

  @ViewChild('three', {read: ElementRef}) canvas!:ElementRef;

  display$!:Observable<unknown>;

  x!:number;
  y!:number;

  constructor(
    
    private three:ThreeService,
    private store:Store

  ) {

    this.display$ = this.three.hover$.asObservable();

  }

  mouseMove ( event:any ) {

    this.x = event.offsetX;
    this.y = event.offsetY;

    const width = this.canvas.nativeElement.clientWidth;
    const height = this.canvas.nativeElement.clientHeight;

    this.three.onMouseMove( event, this.x, this.y, width, height );

  }

  ngOnDestroy ():void {

    this.store.dispatch( resetScene() );

  }

}
