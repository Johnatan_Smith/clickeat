import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_INITIALIZER, Injector, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { CanvasComponent } from './canvas/canvas.component';
import { ControlsComponent } from './controls/controls.component';
import { SingleUnicornComponent } from './single-unicorn/single-unicorn.component';
import { UnicornMatesComponent } from './unicorn-mates/unicorn-mates.component';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input'
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTabsModule } from '@angular/material/tabs';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { ReducerManager, Store, StoreModule } from '@ngrx/store';

import { MAT_COLOR_FORMATS, NgxMatColorPickerModule, NGX_MAT_COLOR_FORMATS } from '@angular-material-components/color-picker';

import { TooltipComponent } from './shared/components/tooltip/tooltip.component';

import { CommonModule } from '@angular/common';

import { EffectsModule } from '@ngrx/effects';
import { UnicornEffects } from './shared/effects/unicorn.effect';
import { SceneEffects } from './shared/effects/scene.effect';
import { SplashComponent } from './splash/splash.component';
import { unicornReducer } from './shared/reducers/unicorn.reducer'


export let appInjector!:Injector;

@NgModule({
  declarations: [
    AppComponent,
    CanvasComponent,
    ControlsComponent,
    TooltipComponent,
    SingleUnicornComponent,
    UnicornMatesComponent,
    SplashComponent
  ],
  imports: [
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    StoreModule.forRoot(
      {},
      { runtimeChecks: {
          strictActionImmutability: false,
          strictStateImmutability: false,
        }
      }
    ),
    EffectsModule.forRoot(
      [
        UnicornEffects,
        SceneEffects
      ]
    ),
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatProgressBarModule,
    NgxMatColorPickerModule,
    MatCardModule,
    MatButtonModule,
    MatTooltipModule,
    MatTabsModule,
    MatStepperModule,
    MatSlideToggleModule,
    MatIconModule,
    MatCheckboxModule,
    CommonModule
  ],
  providers: [ {
      provide: MAT_COLOR_FORMATS,
      useValue: NGX_MAT_COLOR_FORMATS
    },
    {
      provide: APP_INITIALIZER,
      useFactory: unicornReducer,
      deps: [ ReducerManager ],
      multi: true
    }
  ],
  bootstrap: [ AppComponent ]
})

export class AppModule {

  constructor( private injector:Injector ) {

    appInjector = this.injector;

  }

}
