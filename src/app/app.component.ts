import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, NgZone, OnDestroy, Renderer2, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';
import { CanvasComponent } from './canvas/canvas.component';
import { ThreeService } from './shared/services/three.service';
import { LoaderService } from './shared/services/loader.service';
import { Observable, Subscription } from 'rxjs';
import { metaUnicorn } from './shared/interfaces/metaUnicorn';
import { loadScene } from './shared/actions/scene/loadScene.action';
import { themeSelector } from './shared/selectors/unicorn.selector';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class AppComponent implements AfterViewInit, OnDestroy {

  @ViewChild( 'canvas', { read: CanvasComponent } ) comp!:CanvasComponent;
  @ViewChild( 'canvas', { read: ElementRef } ) c!:ElementRef<HTMLElement>;
  
  unicorns:metaUnicorn[] = [];

  isDarkSub!:Subscription;

  isDark$:Observable<boolean> = this.store.select( themeSelector ).pipe(
    tap( isDark => this.toggleTheme( isDark ) )
  )

  loading:boolean = true;

  constructor(

    private three:ThreeService,
    private renderer:Renderer2,
    private loader:LoaderService,
    private store:Store,
    private cdr:ChangeDetectorRef

  ) {

  }

  toggleTheme ( isDark:boolean ):void {

    if ( isDark ) {
    
      this.addClassToBody( 'dark-theme', 'light-theme' );

    } else {
    
      this.addClassToBody( 'light-theme', 'dark-theme' );

    }

  }

  addClassToBody ( classToAdd:string, classToRemove:string ) {

    this.renderer.addClass( document.body, classToAdd );
    this.renderer.removeClass( document.body, classToRemove );

  }

  onResize () {

      console.log( this.c.nativeElement.clientWidth );
      this.three.onWindowResize( this.c.nativeElement );

  }

  get loading$ ():Observable<boolean> {

    return ( this.loader.loadingProject$ );

  }

  ngAfterViewInit() {

    this.isDarkSub = this.isDark$.subscribe();

    this.three.initThree( this.c.nativeElement, this.comp.canvas );

    this.store.dispatch( loadScene() );

  }

  ngOnDestroy () {

    if ( this.isDarkSub )
      this.isDarkSub.unsubscribe();

  }

}
