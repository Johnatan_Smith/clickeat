import { Unicorn } from "../classes/unicorns";
import { metaUnicorn } from "../interfaces/metaUnicorn";

export const setNextPosition = ( unicorns:metaUnicorn[], unicorn:Unicorn, isSingle:boolean, index:number, meta?:metaUnicorn ):void => {

    const numberOfColums = 5;

    if ( meta ) {

        unicorn.setIndexX( meta.indexX );
        unicorn.setIndexY( meta.indexY );
        return;

    }

    if ( !unicorns.length ) {
        
        unicorn.setIndexX( 0 );
        unicorn.setIndexY( 0 );
        return;

    }
      
    let lastIndex = unicorns.length - 1;
    
    if ( unicorns[ lastIndex ].age === 1 )
        lastIndex--;
    
    const lastIndexX = unicorns[ lastIndex ].indexX;

    if ( !isSingle && !index && lastIndexX === numberOfColums - 2 ) {

        unicorn.setIndexX( 0 );
        unicorn.setIndexY( unicorns[ lastIndex ].indexY + 1 );
    
    } else if ( lastIndexX === 4 ) {

        unicorn.setIndexX( 0 );
        unicorn.setIndexY( unicorns[ lastIndex ].indexY + 1 );

    } else {

        unicorn.setIndexX( unicorns[ lastIndex ].indexX + 1 );
        unicorn.setIndexY( unicorns[ lastIndex ].indexY );

    }

}