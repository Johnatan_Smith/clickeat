import { Unicorn } from './../classes/unicorns';
import * as THREE from 'three';
import { appInjector } from "./../../app.module";
import { ThreeService } from '../services/three.service';
import { blendColors } from './blendColors';
import { animateLines } from './animateLine';
import { createLineBetween } from './createLineBetween';

export const connectMates = ( firstUnicorn:Unicorn, secondUnicorn:Unicorn ) => {

    const line = createLineBetween( firstUnicorn, secondUnicorn );

    animateLines( line, firstUnicorn, secondUnicorn );

}