import { Unicorn } from "../classes/unicorns";
import * as THREE from 'three'
import { appInjector } from "src/app/app.module";
import { ThreeService } from "../services/three.service";
import { blendColors } from "./blendColors";
import { Box3, Vector3 } from "three";
import { getCenterPosition } from "./getCenterPosition";

export const createLineBetween = ( firstUnicorn:Unicorn, secondUnicorn:Unicorn ):THREE.Line => {


    const threeService = appInjector.get( ThreeService );

    const firstPosition = getCenterPosition( firstUnicorn );
    const secondPosition = getCenterPosition( secondUnicorn );

    const points = [ firstPosition, secondPosition ];

    const geometry = new THREE.BufferGeometry().setFromPoints( points );

    const color = blendColors( firstUnicorn.getColor(), secondUnicorn.getColor(), 0.5 );
    
    const material = new THREE.LineBasicMaterial( { color } );
    
    const line = new THREE.Line( geometry, material );
  
    threeService.scene.add( line );
    threeService.lines.children.push( line );

    return ( line );

}