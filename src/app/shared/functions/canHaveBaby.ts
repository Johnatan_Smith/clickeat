import { Unicorn } from "../classes/unicorns";
import { Gender } from "../enums/gender";

export const canHaveBaby = ( unicorn:Unicorn, mate:Unicorn ):boolean => {

    const genders = new Set()

    genders.add( unicorn.getGender() );
    genders.add( mate.getGender() );

    return ( genders.has( Gender.male ) && genders.has( Gender.female ) );

}