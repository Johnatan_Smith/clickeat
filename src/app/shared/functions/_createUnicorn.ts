import { FemaleUnicorn, MaleUnicorn, TransgenderUnicorn, Unicorn } from "../classes/unicorns"
import { Gender } from "../enums/gender"

export const _createUnicorn = ( name:string, color:string, gender:Gender, age:number, mate?:Unicorn ): Unicorn => {

    switch ( gender ) {
      
      case Gender.male: return ( new MaleUnicorn( name, color, age,  mate) )
      
      case Gender.female: return ( new FemaleUnicorn( name, color, age, mate ) )
      
      default: return ( new TransgenderUnicorn( name, color, age, mate ) )

    }

}