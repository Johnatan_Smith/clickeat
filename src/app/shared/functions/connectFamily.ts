import { Unicorn } from './../classes/unicorns';
import { connectMates } from './connectMates';

export const connectFamily = ( firstUnicorn:Unicorn, secondUnicorn:Unicorn, baby:Unicorn ) => {

    connectMates( firstUnicorn, baby );
    connectMates( baby, secondUnicorn );

}