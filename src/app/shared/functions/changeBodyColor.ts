import * as THREE from 'three';
import { blendColors } from './blendColors';

export const changeBodyColor = ( model:THREE.Group, color:string ) => {

    const icoSphere = <THREE.Mesh>model.children[ 0 ].children[ 0 ];
    const icoSphere1 = <THREE.Mesh>model.children[ 0 ].children[ 1 ];

    const meshes = [ icoSphere, icoSphere1 ].forEach(

        ( mesh ) => {

            const newColor:string = '#' + color;

            const currentColor:string = '#' + ( mesh.material as any ).color.getHexString();

            const blendedColor:string = blendColors( currentColor, newColor, 0.5 );

            ( mesh.material as any ).color.set( blendedColor );
    
        }
    
    )

}