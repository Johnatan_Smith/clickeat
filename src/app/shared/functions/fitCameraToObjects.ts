import * as THREE from 'three';

export const fitCameraToObjects = ( group:THREE.Group, camera:THREE.PerspectiveCamera ) => {

    const boundingBox = new THREE.Box3();
    boundingBox.setFromObject( group );

    const center = boundingBox.getCenter( new THREE.Vector3() );

    const size = boundingBox.getSize( new THREE.Vector3() );

    camera.position.y = center.y
    camera.position.x = center.x

    var fov = camera.fov * ( Math.PI / 180 );

    var maxDim = Math.max( size.x, size.y );

    const cameraZ = Math.abs( ( maxDim * 1.2  ) * Math.tan( fov * 2 ) );

    camera.position.z = cameraZ;

    camera.lookAt( center );

    camera.updateProjectionMatrix();

}