import { appInjector } from "src/app/app.module";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { Unicorn } from "../classes/unicorns";
import { ThreeService } from "../services/three.service";
import { changeBodyColor } from "./changeBodyColor";
import { fitCameraToObjects } from "./fitCameraToObjects";
import { placeModel } from "./placeModel";
import { connectMates } from "./connectMates";
import * as THREE from 'three';
import { connectFamily } from "./connectFamily";
import { metaUnicorn } from "../interfaces/metaUnicorn";
import { Store } from "@ngrx/store";
import { Group } from "three";
import { switchMap, take } from "rxjs/operators";
import { Observable } from "rxjs";
import { State } from "../interfaces/state";
import { stateSelector } from "../selectors/unicorn.selector";

const setUserDatas = ( model:THREE.Group, unicorn:Unicorn ) => {

    model.userData.name = unicorn.getName();
    model.userData.color = unicorn.getColor();
    model.userData.gender = unicorn.getGender();
    model.userData.age = unicorn.getAge();
    model.userData.isBaby = unicorn.getIsBaby();

}

const animateModel = ( model:THREE.Group, index:number ) => {

    const animCoeff = index !== 2 ? 1 : 3;

    const positionX = model.position.x;
    const positionY = model.position.y;

    const interval = setInterval( () => {

        const time = Date.now() * 0.0005;
        model.position.x = Math.cos( time * animCoeff * 10 ) * 0.05 + positionX;
        model.position.y = Math.cos( time * animCoeff * 7 ) * 0.04 + positionY;
        model.position.z = Math.cos( time * animCoeff * 8 ) * 0.03;

    });

    model.userData.interval = interval;

}

const connectModels = ( unicorn:Unicorn, index:number ) => {

    if ( index === 1 )     
        connectMates( unicorn.getMate(), unicorn ); 
    else if ( index === 2 )
        connectFamily( unicorn.getParents()[ 0 ], unicorn.getParents()[ 1 ], unicorn );

}

const scaleModel = ( model:THREE.Group ) => {

        if ( model.userData.age > 1 )
            model.scale.set(2, 2, 2);
        else
            model.scale.set(1, 1, 1);

}

const initModel = ( unicorn:Unicorn, model:THREE.Group ) => {

    model.rotateY( 3.14 / 1.5 );

    unicorn.setModel( model );

}

export const loadModel = ( context:Unicorn, color:string, isSingle:boolean, index:number, meta?:metaUnicorn ):Promise<Unicorn> => {

    const loader = new GLTFLoader();

    const threeService = appInjector.get( ThreeService );

    const store = appInjector.get( Store );

    const state$ = <Observable<State>>store.select( stateSelector );

    return ( state$.pipe(

        take( 1 ),
        switchMap( state => {

            const unicorns = state.unicorns;

            return ( state.models[ context.getGender() ].arrayBuffer().then(

                ( buf ) => {

                    return ( <Promise<Unicorn>> new Promise( ( resolve ) => loader.parse( buf , '', ( gltf ) => {

                        const model = new Group();

                        model.add( gltf.scene );
                    
                        initModel( context, model );

                        placeModel( unicorns, context, model, isSingle, index, meta );

                        setUserDatas( model, context );

                        changeBodyColor( model, color );

                        scaleModel( model );

                        animateModel( model, index );
                        
                        threeService.globalGroup.add( model );

                        fitCameraToObjects( threeService.globalGroup, threeService.camera );

                        connectModels( context, index );

                        resolve( context );
                
                    } ) ) )
            
                }
                
            ) );

        } )
        
    ).toPromise() )

}