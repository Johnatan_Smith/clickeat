import { Store } from "@ngrx/store";
import { appInjector } from "src/app/app.module";
import { Gender } from "../enums/gender";
import { saveMaleModel } from "../actions/models/saveMaleModel.action";
import { saveFemaleModel } from "../actions/models/saveFemaleModel.action";
import { saveTransgenderModel } from "../actions/models/saveTransgenderModel.action";
import { skipWhile, switchMap, take } from "rxjs/operators";
import { Observable } from "rxjs";
import { modelSelector } from "../selectors/unicorn.selector";

const dispatchByGender = ( gender:Gender, payload:Blob ) => {

    const store = appInjector.get( Store );

    if ( gender === Gender.male )
        store.dispatch( saveMaleModel( { payload } ) );
    else if ( gender === Gender.female )
        store.dispatch( saveFemaleModel( { payload } ) );
    else if ( gender === Gender.transgender )
        store.dispatch( saveTransgenderModel( { payload } ) );

}

const getModel = ( models:any, gender:Gender, path:string ):Promise<undefined>  => {

    if ( models[ gender ] )
        return ( new Promise( resolve => resolve( undefined ) ));

    return ( new Promise( ( resolve, reject ) => {

        var xhr = new XMLHttpRequest();

        xhr.open("GET", path, true);

        xhr.addEventListener("load", function () {

            if (xhr.status === 200) {
                
                const response = xhr.response;

                const blob = new Blob( [ response ] );

                dispatchByGender( gender, blob );

                resolve( undefined );

            } else {

                reject( undefined );

            }

        }, false);

        xhr.send();

    } ) );

}

export const loadToCache = ( path:string, gender:Gender ):Observable<undefined> => {

    const store = appInjector.get( Store );

    const models$ = store.select( modelSelector );

    return ( models$.pipe(

        take( 1 ),
        switchMap( models => getModel( models, gender, path ) ),

    ) );

}