import * as THREE from 'three';
import { PositionalAudio } from 'three';
import { Unicorn } from "../classes/unicorns";

export const getCenterPosition = ( unicorn:Unicorn ):THREE.Vector3 => {

    const model = unicorn.getModel();

    const bbox = new THREE.Box3().setFromObject( model );
    const center = bbox.getCenter( new THREE.Vector3() );
    const position = new THREE.Vector3( model.position.x, center.y, center.z );

    return ( position );

}