import { Unicorn } from "../classes/unicorns";
import * as THREE from 'three';
import { setNextPosition } from "./setNextPosition";
import { metaUnicorn } from "../interfaces/metaUnicorn";

export const placeModel = ( unicorns:metaUnicorn[], context:Unicorn, model:THREE.Group, isSingle:boolean, index:number, meta?:metaUnicorn ) => {

    const bbox = new THREE.Box3().setFromObject( model );
    const size = bbox.getSize( new THREE.Vector3() );

    const sizeY = 3;

    const marginX = size.x / 2;
    const marginY = sizeY * 1.3;

    setNextPosition( unicorns, context, isSingle, index, meta );

    let x = 0;
    let y = 0;

    if ( meta ) {

        x = meta.x;
        y = meta.y;

    } else if ( !context.getIsBaby() ) {

        x = context.getIndexX() * ( size.x + marginX );
        y = -( context.getIndexY() * ( sizeY + marginY ) );

    } else {

        const firstUnicornX = unicorns[ unicorns.length - 2 ].x;
        const secondUnicornX = unicorns[ unicorns.length - 1 ].x;
        const lastUnicornIndexY = unicorns[ unicorns.length - 1 ].indexY;

        x = ( ( secondUnicornX - firstUnicornX ) / 2 ) + firstUnicornX;
        y = -( lastUnicornIndexY * ( sizeY + marginY ) + sizeY / 2 );

    }

    context.setX( x );
    context.setY( y );

    model.position.x = x;
    model.position.y = y;

}