import { Unicorn } from "../classes/unicorns";
import { getCenterPosition } from "./getCenterPosition";

export const animateLines = ( line:THREE.Line, unicorn:Unicorn, mate:Unicorn ) => {

    const interval = setInterval( () => {

        const unicornPosition = getCenterPosition( unicorn );
        const matePosition = getCenterPosition( mate );

        line.geometry.attributes.position.setXY( 0, unicornPosition.x, unicornPosition.y );
        line.geometry.attributes.position.setXY( 1, matePosition.x, matePosition.y );

        line.geometry.attributes.position.needsUpdate = true;

    } );

    line.userData.interval = interval;

}