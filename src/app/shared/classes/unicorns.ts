import { Observable, of } from "rxjs";
import { Gender } from "src/app/shared/enums/gender";
import * as THREE from "three";
import { loadModel } from "../functions/loadModel";
import { loadToCache } from "../functions/loadToCache";
import { metaUnicorn } from "../interfaces/metaUnicorn";

export class Unicorn {

    private unicorn!:Unicorn;
    private mate!:Unicorn;
    private parents:Unicorn[] = []
    private baby!:Unicorn;

    private model!:THREE.Group;

    private x!:number;
    private y!:number;

    private indexX!:number;
    private indexY!:number;

    private isReproducible:boolean = false;
    private isBaby:boolean = false;

    constructor (
        private name:string,
        private color:string,
        private gender:Gender,
        private age:number
    ) {

    }

    getIndexY ():number {

        return ( this.indexY );

    }

    getIndexX ():number {

        return ( this.indexX );

    }

    getX ():number {

        return ( this.x );
    }

    getY ():number {

        return ( this.y );
        
    }

    getUnicorn ():Unicorn {

        return ( this.unicorn );

    }

    getMate ():Unicorn {

        return ( this.mate );

    }

    getParents ():Unicorn[] {

        return ( this.parents );

    }

    getBaby ():Unicorn {

        return ( this.baby );

    }

    getModel ():THREE.Group {

        return ( this.model );

    }

    getName ():string {

        return ( this.name );

    }

    getColor ():string {

        return ( this.color );

    }

    getGender ():Gender {

        return ( this.gender );

    }

    getAge ():number {

        return ( this.age );

    }

    getIsReproducible ():boolean {

        return ( this.isReproducible );

    }

    getIsBaby ():boolean {

        return ( this.isBaby );

    }

    setIndexX ( indexX:number ) {

        this.indexX = indexX;

    }

    setIndexY ( indexY:number ) {

        this.indexY = indexY;

    }

    setX ( x:number ):void {

        this.x = x;
    }

    setY ( y:number ):void {

        this.y = y;
        
    }

    setMate ( mate:Unicorn ) {

        if ( !this.mate ) {
            this.mate = mate;
            mate.setMate( this );
        }

    }

    setParents ( firstUnicorn:Unicorn, secondUnicorn:Unicorn ) {

        this.parents = [ firstUnicorn, secondUnicorn ];

    }

    setBaby ( baby:Unicorn ) {

        this.baby = baby;
        if ( !this.mate.getBaby() )
            this.mate.setBaby( baby );
    }

    setModel = ( model:THREE.Group ) => {

        this.model = model;
    
    }

    setName ( name:string ):void {
        
        this.name = name;

    }

    setColor ( color:string ):void {
        
        this.color = color;

    }

    setGender ( gender:Gender):void {

        this.gender = gender;

    }

    setAge ( age:number ):void {
        
        this.age = age;

    }

    setIsReproducible ( value:boolean ) {

        this.isReproducible = value;

    }

    setIsBaby ( value:boolean ) {

        this.isBaby = value;

    }

    create ( isSingle?:boolean, index?:number, meta?:metaUnicorn ):Promise<Unicorn> {

        return ( new Promise( resolve => resolve( this ) ) )
    
    }

    loadToCache ():Observable<undefined> {

        return ( of( undefined ) );

    }

}

export class TransgenderUnicorn extends Unicorn {

    public path = 'assets/gltf_models/transgender_unicorn.gltf';

    constructor (
        name:string,
        color:string,
        age:number,
        mate?:Unicorn
    ) {

        super( name, color, Gender.transgender, age );
        
        if ( mate ) this.setMate( mate );

    }

    create ( isSingle:boolean, index:number, meta?:metaUnicorn ):Promise<Unicorn> {

        return ( loadModel( this, this.getColor(), isSingle, index, meta ) )
    }

    loadToCache ():Observable<undefined> {

        return ( loadToCache( this.path, this.getGender() ) );

    }

}

export class MaleUnicorn extends Unicorn {

    public path = 'assets/gltf_models/male_unicorn.gltf';

    constructor (
        name:string,
        color:string,
        age:number,
        mate?:Unicorn
    ) {

        super( name, color, Gender.male, age );

        if ( mate ) this.setMate( mate );

    }

    create ( isSingle:boolean, index:number, meta?:metaUnicorn ):Promise<Unicorn> {

        return ( loadModel( this, this.getColor(), isSingle, index, meta ) )
        

    }

    loadToCache ():Observable<undefined> {

        return ( loadToCache( this.path, this.getGender() ) );

    }

}

export class FemaleUnicorn extends Unicorn {

    public path = 'assets/gltf_models/female_unicorn.gltf';

    constructor (
        name:string,
        color:string,
        age:number,
        mate?:Unicorn
    ) {

        super( name, color, Gender.female , age );

        if ( mate ) this.setMate( mate );

    }

    create ( isSingle:boolean, index:number, meta?:metaUnicorn ):Promise<Unicorn> {

        return ( loadModel( this, this.getColor(), isSingle, index, meta ) )
    
    }

    loadToCache ():Observable<undefined> {

        return ( loadToCache( this.path, this.getGender() ) );

    }

}