import { createSelector } from "@ngrx/store";

export const stateSelector = createSelector(
    state => ( state as any ).reducer,
    state => state
)

export const unicornsSelector = createSelector(
    state => ( state as any ).reducer,
    state => state.unicorns
)

export const modelSelector = createSelector(
    state => ( state as any ).reducer,
    state => state.models
)

export const themeSelector = createSelector(
    state => ( state as any ).reducer,
    state => state.isDark
)