import { on, ReducerManager } from "@ngrx/store";
import { createUnicorn } from "../actions/unicorns/createUnicorn.action";
import { createMates } from "../actions/unicorns/createMates.action";
import { metaUnicorn } from "../interfaces/metaUnicorn";
import { createIndexedDBRehydrateReducer } from "../utils/rehydrate/indexedDb";
import { saveUnicorn } from "../actions/unicorns/saveUnicorn.action";
import { initialState } from "../states/initialState.state";
import { saveUnicorns } from "../actions/unicorns/saveUnicorns.action";
import { State } from "../interfaces/state";
import { resetTheme } from "../actions/theme/resetTheme.action";
import { setTheme } from "../actions/theme/setTheme.action";
import { loadScene } from "../actions/scene/loadScene.action";
import { resetScene } from "../actions/scene/resetScene.action";
import { saveMaleModel } from "../actions/models/saveMaleModel.action";
import { saveFemaleModel } from "../actions/models/saveFemaleModel.action";
import { saveTransgenderModel } from "../actions/models/saveTransgenderModel.action";

export function unicornReducer ( reducerManager: ReducerManager ) { 
    return async () => {
    
        const reducer = await createIndexedDBRehydrateReducer(

            'bigState',
            initialState,
            on( createUnicorn, ( state:unknown ) => state ),
            on( createMates, ( state:unknown ) => state ),
            on( saveUnicorn, ( state:any, action:{ payload:metaUnicorn } ) => {
                
                state.unicorns.push( action.payload )

                return ( state );
            
            } ),
            on( saveUnicorns, ( state:any, action:{ payload:metaUnicorn[] } ) => {

                state.unicorns = [...state.unicorns, ...action.payload ]

                return ( state );

            } ),
            on( resetTheme, ( state:unknown ) => ( { ...(state as State), isDark: false } ) ),
            on( setTheme, ( state:unknown, action: { payload:boolean }) => ( { ...(state as State), isDark: action.payload } ) ),
            on( loadScene, state => state ),
            on( resetScene, ( state:any ) => {
            
                state.unicorns = []
        
                return ( state );
            
            } ),
            on( saveMaleModel, ( state:unknown, action:{ payload:Blob } ) => {
                
                ( state as State ).models.male = action.payload
                
                return ( state );

            } ),
            on( saveFemaleModel, ( state:unknown, action:{ payload:Blob } ) => {
                
                ( state as State ).models.female = action.payload
                
                return ( state );

            } ),
            on( saveTransgenderModel, ( state:unknown, action:{ payload:Blob } ) => {
                
                ( state as State ).models.transgender = action.payload
                
                return ( state );

            } )

        )

        reducerManager.addReducer(
            'reducer',
            reducer
        )

    }

};
