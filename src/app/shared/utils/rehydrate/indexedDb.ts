import { TitleCasePipe } from "@angular/common";
import { Action, ActionCreator, ActionReducer, createReducer, Creator } from "@ngrx/store";
import { OnReducer } from "@ngrx/store/src/reducer_creator";
import { State } from "../../interfaces/state";

export const getInitialStateFromDb = async ( db: IDBDatabase|undefined, reducerKey: string ) => {

    return new Promise( ( resolve, reject ) => {
    
        if ( db === undefined ) return ( reject() );

        const stateObjectStore = db.transaction( reducerKey, "readonly").objectStore( reducerKey );
        
        const request = stateObjectStore.get( "1" );
        
        request.onerror = reject;
        
        request.onsuccess = function () {

        resolve(request.result);

        };

    } );

}

const tryCreateStore = async ( request: IDBOpenDBRequest, reducerKey: string ) => {
  
    return ( new Promise( ( resolve, reject ) => {
        
        request.onupgradeneeded = (event: IDBVersionChangeEvent) => {
      
            const db = request.result;
        
            if (!db.objectStoreNames.contains( reducerKey ) ) {
            
                db.createObjectStore( reducerKey );
            
            }
    
        };

        request.onsuccess = () => {
        
            resolve( request.result );

        };
        
        request.onerror = reject;

    } ) );

}

export const saveState = async ( db: IDBDatabase|undefined, newState: any, reducerKey: string ) => {

  return new Promise( ( resolve, reject ) => {

        if ( db === undefined ) return ( reject() );

        const stateObjectStore = db.transaction( reducerKey, "readwrite" ).objectStore( reducerKey );
        
        const request = stateObjectStore.put( newState, "1" );
        
        request.onerror = reject;
        request.onsuccess = resolve;
  
    });

}

export async function createIndexedDBRehydrateReducer<S extends State, A extends Action = Action>(
  reducerKey: string,
  initialState: S,
  ...ons: any[]
): Promise<ActionReducer<S, A>> {

    const request = window.indexedDB.open( "Unicorn" );
    
    let db:IDBDatabase|undefined = undefined;

    let newInitialState;

    try {

        db = <IDBDatabase> await tryCreateStore(request, reducerKey);

    } catch (err) {
    
        alert( 'Error: Please Disable Private Mode or Enable History in your Browser settings' );
    
    }

    newInitialState = ( ( await getInitialStateFromDb( db, reducerKey ) ) as S) ?? initialState;

    const newOns:any = [];

    ons.forEach((oldOn: any) => {

        const newReducer: ActionReducer<S, A> = ( state: S | undefined, action: A ) => {

        const newState = oldOn.reducer(state, action);
        
        saveState( db, newState, reducerKey).then(() => undefined );

        return newState;

    };

        newOns.push({ ...oldOn, reducer: newReducer });
  
    });

    return Promise.resolve( createReducer(newInitialState, ...newOns ) );

}