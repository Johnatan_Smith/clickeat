import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { from, of } from 'rxjs';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { loadScene } from '../actions/scene/loadScene.action';
import { resetScene } from '../actions/scene/resetScene.action';
import { unicornsSelector } from '../selectors/unicorn.selector';
import { CacheService } from '../services/cache.service';
import { LoaderService } from '../services/loader.service';
import { ThreeService } from '../services/three.service';
 
@Injectable()
export class SceneEffects {

    loadScene$ = createEffect( () => this.actions$.pipe(

        ofType( loadScene ),
        tap( () => this.loader.loadingCanvas$.next( true ) ),
        switchMap( _ => this.three.loadToCache() ),
        tap( () => this.cache.isCached$.next( true ) ),
        switchMap( _ => this.store.select( unicornsSelector ) ),
        switchMap( unicorns => from( this.three.loadScene( unicorns ) ) ),
        tap( _ => this.loader.loadingCanvas$.next( false ) )

    ), { dispatch:false } );

    resetAll$ = createEffect( () => this.actions$.pipe(

      ofType( resetScene ),
      map( _ => this.three.resetAll() )

    ), { dispatch:false } )

  constructor(

    private actions$: Actions,
    private loader:LoaderService,
    private cache:CacheService,
    private three:ThreeService,
    private store:Store

  ) {}

}