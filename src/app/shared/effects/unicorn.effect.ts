import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY, from, of } from 'rxjs';
import { map, catchError, switchMap, tap, finalize } from 'rxjs/operators';
import { createMates } from '../actions/unicorns/createMates.action';
import { createUnicorn } from '../actions/unicorns/createUnicorn.action';
import { saveUnicorn } from '../actions/unicorns/saveUnicorn.action';
import { saveUnicorns } from '../actions/unicorns/saveUnicorns.action';
import { FormService } from '../services/form.service';
import { LoaderService } from '../services/loader.service';
import { UnicornService } from '../services/unicorn.service';
 
@Injectable()
export class UnicornEffects {

    createUnicorn$ = createEffect( () => this.actions$.pipe(

        ofType( createUnicorn ),
        tap( _ => this.loader.loading$.next( true ) ),
        map( properties => from( this.us.createUnicorn( properties.payload , true  ) ) ),
        catchError(() => EMPTY ),
    
    ), { dispatch:false } );

    createMates$ = createEffect( () => this.actions$.pipe(

        ofType( createMates ),
        tap( _ => this.loader.loading$.next( true ) ),
        map( ( properties:any ) => from( this.us.createMates( 
            properties.payload.unicorn,
            properties.payload.mate,
            properties.payload.haveBaby
        ) ) ),
        catchError( () => EMPTY )

    ), { dispatch:false } )

    save$ = createEffect( () => this.actions$.pipe(
        ofType( saveUnicorn, saveUnicorns ),
        tap( () => {

            this.form.submitForm$.next( true )
            this.loader.loading$.next( false )

        })
    ), { dispatch: false } )

  constructor(

    private actions$: Actions,
    private us:UnicornService,
    private form:FormService,
    private loader:LoaderService

  ) {}
}