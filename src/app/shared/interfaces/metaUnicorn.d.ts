import { Gender } from "../enums/gender";

export interface metaUnicorn {

    uuid:string,

    name:string,
    color:string,
    gender:Gender,
    age:number,
    
    x:number,
    y:number,

    indexX:number,
    indexY:number,

    isBaby:boolean,

    isReproducible?:boolean,

    mateUUID?:string

    parent1UUID?:string,
    parent2UUID?:string,
    babyUUID?:string

}