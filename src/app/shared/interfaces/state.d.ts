import { Unicorn } from "src/app/shared/classes/unicorns";
import { metaUnicorn } from "./metaUnicorn";

export interface State {
    isDark:boolean,
    unicorns: metaUnicorn[];
    models: {
        male:Blob,
        female:Blob,
        transgender:Blob
    }
}