import { Gender } from "../enums/gender";

export interface ActionUnicorn {

    name:string
    color:string,
    gender:Gender,
    age:number

}