import { Component, Input, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-tooltip',
  templateUrl: './tooltip.component.html',
  styleUrls: ['./tooltip.component.sass'],
  encapsulation: ViewEncapsulation.None
})
export class TooltipComponent  {

  @ViewChild('tooltip') divEl!:ElementRef<HTMLElement>;

  unicorn!:THREE.Object3D|undefined;

  @Input('x')x!:number;

  @Input('y')y!:number;

  @Input('display') set display ( value:unknown ) {

    if ( this.divEl && this.unicorn !== value && value ) {
  
      this.unicorn = <THREE.Mesh>value;

      let offsetY = 0;

      if ( this.y + 380 > window.innerHeight - 50 )
        offsetY = -380;
      if ( this.y - 380 < 0 )
        offsetY = 0 - this.y;

      this.divEl.nativeElement.style.top = this.y + offsetY + "px";
      this.divEl.nativeElement.style.left = this.x + 100 + "px";

    } else if ( this.divEl && !value ) {

      this.unicorn = <undefined>value;

    }

  };

  constructor( ) {

  }

  get name ():string {

    return ( this.unicorn ? ( this.unicorn as any ).parent.parent.userData.name:'');

  }

  get color ():string {

    return ( this.unicorn ? ( this.unicorn as any ).parent.parent.userData.color:'');

  }

  get gender ():string {

    return ( this.unicorn ? ( this.unicorn as any ).parent.parent.userData.gender: '');

  }

  get age ():number {

    return ( this.unicorn ? ( this.unicorn as any ).parent.parent.userData.age: 0 );

  }

  get isBaby ():string {

    return ( this.unicorn ? ( this.unicorn as any ).parent.parent.userData.isBaby: false );

  }

}
