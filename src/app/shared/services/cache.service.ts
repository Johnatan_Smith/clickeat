import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CacheService {

  isCached$:BehaviorSubject<boolean> = new BehaviorSubject( false as boolean );

  constructor() { }
}
