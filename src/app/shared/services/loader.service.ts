import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  loading$ = new BehaviorSubject( false );
  loadingCanvas$ = new BehaviorSubject( true );
  loadingProject$ = new BehaviorSubject( true );

  constructor() {
  
  }

}
