import { Injectable } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  submitForm$ = new BehaviorSubject<boolean>( false );

  constructor() { }

}
