import { ElementRef, Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import * as THREE from 'three';
import { FemaleUnicorn, MaleUnicorn, TransgenderUnicorn, Unicorn } from '../classes/unicorns';
import { canHaveBaby } from '../functions/canHaveBaby';
import { _createUnicorn } from '../functions/_createUnicorn';
import { metaUnicorn } from '../interfaces/metaUnicorn';
import { modelSelector, unicornsSelector } from '../selectors/unicorn.selector';

@Injectable({
  providedIn: 'root'
})
export class ThreeService {

  renderer!:THREE.WebGLRenderer;
  
  camera!:THREE.PerspectiveCamera;
  
  scene:THREE.Scene = new THREE.Scene();

  rayCaster:THREE.Raycaster = new THREE.Raycaster();

  hover$:BehaviorSubject<THREE.Object3D> = new BehaviorSubject( undefined as any );

  hover:boolean = false;

  globalGroup!:THREE.Group;

  width:number = 0;
  height:number = 0;

  lines:{ children:THREE.Line[] } = { children:[] };

  models$:Observable<Blob> = this.store.select( modelSelector );
  unicorns$:Observable<metaUnicorn[]> = this.store.select( unicornsSelector );

  constructor ( private store:Store ) { }

  initThree ( cont:HTMLElement, el:ElementRef<HTMLCanvasElement> ):void {

    const canvas = <HTMLCanvasElement> el.nativeElement;

    this.width = cont.clientWidth;
    this.height = cont.clientHeight;

    this.renderer = new THREE.WebGLRenderer( { canvas } );
    this.renderer.setSize( this.width, this.height );
    this.renderer.shadowMap.enabled = true;
    
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;
  
    this.scene.add( new THREE.HemisphereLight( 0x606060, 0x404040 ) );
    const light = new THREE.DirectionalLight( 0xffffff );
    light.position.set( 1, 1, 1 ).normalize();
    
    this.scene.add( light );
    
    const zPosition = width;
    
    this.camera = new THREE.PerspectiveCamera(
  
      70,
      width / height,
      0.1,
      1000

    )

    this.camera.position.set(0, 0, 4);

    this.camera.userData.fov = this.camera.fov;

    this.globalGroup = new THREE.Group();

    this.scene.add( this.globalGroup );

    this.scene.background = new THREE.Color( 0x878baa );

    this.onWindowResize( cont );

    this.renderer.setAnimationLoop( this.animate );

  }

  intersectObject( x:number, y:number ) {

      const mouse = new THREE.Vector2( x, y );

      this.rayCaster.setFromCamera( mouse, this.camera );

      const threshold = 3;

      this.rayCaster.params.Line = { threshold };

      const intersects = this.rayCaster.intersectObjects( this.globalGroup.children , true );

      if ( intersects.length && ( this.hover$.value?.parent !== intersects[ 0 ].object.parent || !this.hover ) ) {

        this.hover$.next( intersects[ 0 ].object );
        this.hover = true;

      } else if ( !intersects.length && this.hover ){

        this.hover$.next( undefined as any );
        this.hover = false;

      }

  }

  onMouseMove ( event:MouseEvent, offsetX:number, offsetY:number, width:number, height:number ) {

    const x = ( offsetX / width ) * 2 - 1;
	  const y = - ( offsetY / height ) * 2 + 1;

    this.intersectObject( x, y );

  }

  animate = () => {

      this.renderer.render( this.scene, this.camera );
  
  }

  onWindowResize( el:HTMLElement ):void {

    if ( !el ) return;

    this.width = el.clientWidth;
    this.height = el.clientHeight;

    (this.camera as any ).aspect = this.width / this.height;

    (this.camera as any ).updateProjectionMatrix();

    this.renderer.setSize( this.width, this.height );

  }

  freeNodes( node:any ){

    if (!node) return;

    if ( node?.userData?.interval )
      clearInterval( node.userData.interval );
      
    for ( const obj of node.children )
      this.freeNodes( obj );

    if (node.geometry)
      node.geometry.dispose();
    
    if ( node.material ){

      for (const key of Object.keys( node.material) ) {

        const value = node.material[key];

        if ( value && typeof value === 'object' && 'minFilter' in value)
          value.dispose();
      
      }

      node.material.dispose();    

    }

  }

  loadUnicorn ( unicorns:metaUnicorn[], index:number[], unicorn:Unicorn[] ):Promise<Unicorn> {

    const meta = unicorns[ index[ 0 ] ];

    unicorn[ 0 ] = _createUnicorn( meta.name, meta.color, meta.gender, meta.age, );

    index[ 0 ]++

    return ( unicorn[ 0 ].create( !!meta.mateUUID, 0, meta ) );
  
  }

  loadMate ( unicorns:metaUnicorn[], index:number[], unicorn:Unicorn, mate:Unicorn[]):Promise<Unicorn|undefined> {

    if ( !unicorns[ index[ 0 ] ] || !unicorns[ index[ 0 ] ].mateUUID )
      return ( new Promise( resolve => resolve( undefined ) ) );

    const meta = unicorns[ index[ 0 ] ];

    mate[ 0 ] = _createUnicorn( meta.name, meta.color, meta.gender, meta.age, unicorn );

    if ( canHaveBaby( unicorn, mate[ 0 ] ) ) {

      mate[ 0 ].setIsReproducible( true );
      unicorn.setIsReproducible( true );

    }

    index[ 0 ]++;

    return ( mate[ 0 ].create( false , 1, meta ) );
  
  }

  loadBaby ( unicorns:metaUnicorn[], index:number[], unicorn:Unicorn, mate:Unicorn ):Promise<Unicorn|undefined> {

    if ( !unicorns[ index[ 0 ] ] || !unicorns[ index[ 0 ] ].isBaby )
      return ( new Promise( resolve => resolve( undefined ) ) );

    const meta = unicorns[ index[ 0 ] ];

    const baby = _createUnicorn( meta.name, meta.color, meta.gender, meta.age );

    unicorn.setBaby( baby );
    mate.setBaby( baby );
    
    baby.setIsBaby( true );
    baby.setParents( unicorn, mate );

    index[ 0 ]++;

    return ( baby.create( false , 2, meta ) );
  
  }

  async loadScene ( unicorns:metaUnicorn[], index: number[] = [ 0 ] ):Promise<undefined> {

    if ( index[ 0 ] >= unicorns.length )
      return ( new Promise( resolve => resolve( undefined ) ) );

    let unicorn:Unicorn[] = [];
    let mate:Unicorn[] = [];

    while ( index[ 0 ] < unicorns.length ) {

      await this.loadUnicorn( unicorns, index, unicorn )
      .then( _ => this.loadMate( unicorns, index, unicorn[ 0 ], mate ) )
      .then( _ => this.loadBaby( unicorns, index, unicorn[ 0 ], mate[ 0 ] ) )
    
    }

    return ( Promise.resolve( undefined ) );
  
  }

  loadToCache ():Observable<undefined> {

    const male = new MaleUnicorn( '', '', 0 )
    const female = new FemaleUnicorn( '', '', 0 )
    const transgender = new TransgenderUnicorn( '', '', 0 )

    return (

      this.models$.pipe(

        switchMap( _ => male.loadToCache() ),
        switchMap( _ => female.loadToCache() ),
        switchMap( _ => transgender.loadToCache() ),

      )

    )

  }

  resetAll ():void {
    
    this.freeNodes( this.globalGroup );
    this.freeNodes( this.lines );

    while ( this.globalGroup.children.length > 0)
      this.globalGroup.remove( this.globalGroup.children[0] );
    
    for ( let i = this.scene.children.length - 1; i >= 0; i--) {
      if ( this.scene.children[ i ].type === 'Line' )
        this.scene.remove( this.scene.children[ i ] );
    }

    this.lines.children = [];

  }

}
