import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { saveUnicorn } from '../actions/unicorns/saveUnicorn.action';
import { Unicorn } from '../classes/unicorns';
import { Gender } from '../enums/gender';
import { blendColors } from '../functions/blendColors';
import { canHaveBaby } from '../functions/canHaveBaby';
import { _createUnicorn } from '../functions/_createUnicorn';
import { ActionUnicorn } from '../interfaces/actionUnicorn.interface';
import { metaUnicorn } from '../interfaces/metaUnicorn';

@Injectable({
  providedIn: 'root'
})
export class UnicornService {

  constructor( private store:Store ) { }

  getMetaUnicorn ( unicorn:Unicorn ):metaUnicorn {

    const mateUUID = ( unicorn.getMate() ) ? unicorn.getMate().getModel().uuid : undefined;

    let isBaby:boolean;
    let isReproducible:boolean = ( unicorn.getMate() ) ? canHaveBaby( unicorn, unicorn.getMate() ) : false;
    let parent1UUID:string = undefined!;
    let parent2UUID:string = undefined!;

    if ( isBaby = unicorn.getIsBaby() ) {

      const parents = unicorn.getParents();

      parent1UUID = parents[ 0 ].getModel().uuid;
      parent2UUID = parents[ 1 ].getModel().uuid;

    }

    return (
      {

        uuid:unicorn.getModel().uuid,

        name: unicorn.getName(),
        color: unicorn.getColor(),
        gender: unicorn.getGender(),
        age: unicorn.getAge(),

        x: unicorn.getX(),
        y: unicorn.getY(),

        indexX: unicorn.getIndexX(),
        indexY: unicorn.getIndexY(),

        isBaby,
        isReproducible,

        mateUUID,

        parent1UUID,
        parent2UUID,

      }
    
    )
  
  }

  saveUnicorn ( unicorn:Unicorn ):Unicorn {

    const payload = this.getMetaUnicorn( unicorn );

    this.store.dispatch( saveUnicorn( { payload } ) );

    return ( unicorn );

  }

  createMates ( unicorn:ActionUnicorn, mate:ActionUnicorn, haveBaby:false ):Promise<Unicorn> {

    let firstUnicorn:Unicorn;

    return ( 
      
      this.createUnicorn( unicorn, false )
      .then( u => firstUnicorn = u )
      .then( _ => this.createMate( mate, firstUnicorn, haveBaby ) )
      .then( mateUnicorn => this.createBaby( firstUnicorn , mateUnicorn , haveBaby ) )

    )
  
  }

  createUnicorn ( properties:ActionUnicorn, isSingle:boolean ):Promise<Unicorn> {

    const unicorn = _createUnicorn( properties.name, properties.color, properties.gender, properties.age );

    return (

      unicorn.create( isSingle, 0 )
      .then( u => this.saveUnicorn( u ) )
    
    );

  }

  createMate ( properties:ActionUnicorn, mate:Unicorn, haveBaby:boolean ):Promise<Unicorn> {

    const unicorn = _createUnicorn( properties.name, properties.color, properties.gender, properties.age, mate );

    if ( haveBaby ) {

      mate.setIsReproducible( true );
      unicorn.setIsReproducible( true );
    
    }

    return (
      
      unicorn.create( false, 1 )
      .then( u => this.saveUnicorn( u ))
    
    );

  }

  createBaby( unicorn:Unicorn, mate:Unicorn, haveBaby:boolean ):Promise<Unicorn> {

    if ( !haveBaby )
      return ( undefined! )

    const name = unicorn.getName() + mate.getName();
    const color = blendColors( unicorn.getColor(), mate.getColor(), 0.5 ).substring( 1 );
    const gender = Object.values( Gender )[ Math.floor( Math.random() * 3 ) ];
    const age = 1;

    const baby = _createUnicorn( name, color, gender, age );

    unicorn.setBaby( baby );
    mate.setBaby( baby );
    
    baby.setIsBaby( true );
    baby.setParents( unicorn, mate );

    return (
    
      baby.create( false, 2 )
      .then( u => this.saveUnicorn( u ) )

    );

  }

}
