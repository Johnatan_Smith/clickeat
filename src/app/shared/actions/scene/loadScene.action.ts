import { createAction } from "@ngrx/store";

export const loadScene = createAction('[Models API] Load Scene' );