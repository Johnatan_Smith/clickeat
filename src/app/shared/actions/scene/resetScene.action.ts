import { createAction } from "@ngrx/store";

export const resetScene = createAction('[Models API] Reset Scene' );