import { createAction, props } from "@ngrx/store";

export const setTheme = createAction('[Controls Component] Set Theme', props<{ payload:boolean }>());