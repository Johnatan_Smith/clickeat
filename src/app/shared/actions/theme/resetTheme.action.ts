import { createAction } from "@ngrx/store";

export const resetTheme = createAction('[Theme API] Reset Theme');