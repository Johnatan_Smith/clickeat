import { createAction, props } from "@ngrx/store";
import { ActionUnicorn } from "../../interfaces/actionUnicorn.interface";

export const createMates = createAction('[Controls Component] Create Mates', props<{ payload: { unicorn:ActionUnicorn, mate:ActionUnicorn, haveBaby:boolean} }>());