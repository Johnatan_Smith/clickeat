import { createAction, props } from "@ngrx/store";
import { ActionUnicorn } from "../../interfaces/actionUnicorn.interface";
import { metaUnicorn } from "../../interfaces/metaUnicorn";

export const saveUnicorn = createAction('[Unicorn API] Save Unicorn', props<{ payload:metaUnicorn }>());