import { createAction, props } from "@ngrx/store";
import { metaUnicorn } from "../../interfaces/metaUnicorn";

export const saveUnicorns = createAction('[Unicorn API] Save Unicorns', props<{ payload:metaUnicorn[] }>());