import { createAction, props } from "@ngrx/store";
import { ActionUnicorn } from "../../interfaces/actionUnicorn.interface";

export const createUnicorn = createAction('[Controls Component] Create Unicorn', props<{ payload:ActionUnicorn }>());