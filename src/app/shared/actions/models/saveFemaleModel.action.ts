import { createAction, props } from "@ngrx/store";

export const saveFemaleModel = createAction('[Models API] Save Female Model', props<{ payload:Blob }>());