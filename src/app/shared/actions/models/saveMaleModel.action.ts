import { createAction, props } from "@ngrx/store";

export const saveMaleModel = createAction('[Models API] Save Male Model', props<{ payload:Blob }>());