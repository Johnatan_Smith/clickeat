import { createAction, props } from "@ngrx/store";

export const saveTransgenderModel = createAction('[Models API] Save Transgender Model', props<{ payload:Blob }>());