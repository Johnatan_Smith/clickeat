import { State } from "../interfaces/state";

export const initialState:State = {
    isDark: false,
    unicorns: [],
    models: {
        male:undefined!,
        female:undefined!,
        transgender:undefined!
    }
}