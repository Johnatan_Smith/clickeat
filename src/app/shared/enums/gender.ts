export enum Gender {

    male = "male",
    female = "female",
    transgender = "transgender"

}