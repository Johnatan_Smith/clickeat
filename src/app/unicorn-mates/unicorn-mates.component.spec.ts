import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnicornMatesComponent } from './unicorn-mates.component';

describe('UnicornMatesComponent', () => {
  let component: UnicornMatesComponent;
  let fixture: ComponentFixture<UnicornMatesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnicornMatesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UnicornMatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
