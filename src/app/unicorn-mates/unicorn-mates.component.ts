import { Component, Input, AfterContentInit, ViewChild, AfterViewInit, OnDestroy, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { MatStepper } from '@angular/material/stepper';
import { BehaviorSubject, Subscription } from 'rxjs';
import { skipWhile, tap } from 'rxjs/operators';
import { Unicorn } from '../shared/classes/unicorns';
import { Gender } from '../shared/enums/gender';
import { FormService } from '../shared/services/form.service';
import { LoaderService } from '../shared/services/loader.service';

@Component({
  selector: 'app-unicorn-mates',
  templateUrl: './unicorn-mates.component.html',
  styleUrls: ['./unicorn-mates.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UnicornMatesComponent implements AfterContentInit, AfterViewInit, OnDestroy {

  @ViewChild('stepper') stepper!:MatStepper;

  @Input('firstFormGroup') firstFormGroup!:FormGroup;
  @Input('secondFormGroup') secondFormGroup!:FormGroup;

  @Output('onBaby') onBaby = new EventEmitter<boolean>();

  public disabled = false;
  public color: ThemePalette = 'primary';
  public touchUi = false;
  public unicorn!:Unicorn|undefined;
  public formsInitialized = false;

  public submitSub!:Subscription;

  private _haveBaby:boolean = false;

  genders = Object.values( Gender );

  constructor(
    private loader:LoaderService,
    private form:FormService
  ) {

    this.submitSub = this.form.submitForm$.pipe(
      skipWhile( isSubmitted => !isSubmitted),
      tap( _ => this.goToFirst() ),
      tap( _ => this.resetForms() )
    ).subscribe();

  }

  set haveBaby ( value:boolean ) {

    this.onBaby.emit( value );
    this._haveBaby = value;

  }

  get haveBaby ():boolean {

    return ( this._haveBaby );

  }

  canHaveBaby () {

    const genderSet = new Set();

    const gender1 = this.firstFormGroup.controls?.gender?.value;
    const gender2 = this.secondFormGroup.controls?.gender?.value;

    genderSet.add( gender1 );
    genderSet.add( gender2 );

    return ( genderSet.has( Gender.male ) && genderSet.has( Gender.female ) );

  }

  resetForms ():void {

      this.firstFormGroup.markAsUntouched();
      this.firstFormGroup.markAsPristine();
      this.secondFormGroup.markAsUntouched();
      this.secondFormGroup.markAsPristine();
      this.haveBaby = false;

  }

  stepTouched() {

    ( this.stepper.selected as any ).interacted = false;
  
  }

  goToFirst ():void {

    setTimeout( () => this.stepper.selectedIndex = 0 );

  }

  get loading$ ():BehaviorSubject<boolean> {
    
    return ( this.loader.loading$ )
  
  }

  ngAfterContentInit () {

    this.formsInitialized = true;
  
  }

  ngAfterViewInit () {

    this.stepTouched();

  }

  ngOnDestroy ():void {

    if ( this.submitSub )
      this.submitSub.unsubscribe();

  }


}
