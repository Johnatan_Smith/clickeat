import { Component, Input, ChangeDetectorRef, AfterContentInit, ViewChild, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, Validators } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { skipWhile, tap } from 'rxjs/operators';
import { Unicorn } from '../shared/classes/unicorns';
import { Gender } from '../shared/enums/gender';
import { FormService } from '../shared/services/form.service';
import { LoaderService } from '../shared/services/loader.service';

@Component({
  selector: 'app-single-unicorn',
  templateUrl: './single-unicorn.component.html',
  styleUrls: ['./single-unicorn.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SingleUnicornComponent implements AfterContentInit, OnDestroy {

  @Input('formGroup') formGroup!:FormGroup;

  @ViewChild('formDirective') formDirective!:FormGroupDirective;

  public disabled = false;
  public color: ThemePalette = 'primary';
  public touchUi = false;
  public unicorn!:Unicorn|undefined;
  public formInitialized = false;

  public submitSub!:Subscription;

  genders = Object.values( Gender );

  constructor(

    private loader:LoaderService,
    private cdr:ChangeDetectorRef,
    private form:FormService

  ) {

    this.submitSub = this.form.submitForm$.pipe(
      skipWhile( isSubmitted => !isSubmitted ),
      tap( _ => this.resetForm() )
    ).subscribe()

  }

  resetForm () {

    this.formGroup.reset();
    this.formGroup.controls[ 'age' ].setValue( 2 );

  }

  get loading$ ():BehaviorSubject<boolean> {
    
    return ( this.loader.loading$ )
  
  }

  submit ( formDirective:FormGroupDirective ) {

    formDirective.resetForm();

  }

  initForm ():void {

    this.formGroup.addControl( 'name', new FormControl( { value:'', disabled:false }, Validators.required ) );
    this.formGroup.addControl( 'color', new FormControl( { value: undefined, disabled:false }, Validators.required ) );
    this.formGroup.addControl( 'gender', new FormControl( { value:'', disabled:false }, Validators.required ) );
    this.formGroup.addControl( 'age', new FormControl( { value: 2, disabled:false }, Validators.required ) );

  }

  ngAfterContentInit () {

    this.initForm();

    this.formInitialized = true;
  
    this.cdr.detectChanges();

  }

  ngOnDestroy () {

    if ( this.submitSub )
      this.submitSub.unsubscribe();

  }

}
