import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleUnicornComponent } from './single-unicorn.component';

describe('SingleUnicornComponent', () => {
  let component: SingleUnicornComponent;
  let fixture: ComponentFixture<SingleUnicornComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleUnicornComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleUnicornComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
