FROM alpine:3.14

ENV NODE_VERSION 14.15

RUN apk add --update nodejs npm

WORKDIR usr/app

COPY package*.json ./

RUN npm install -g @angular/cli @angular-devkit/build-angular --allow-dirty && npm install

EXPOSE 4201

CMD [ "npm", "start" ]